#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Bettina class. Combining land and flora object and initializing the time
stepping within the model.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""


class Bettina:
    def __init__(self, name, land, flora):
        self.setup_name = name
        self.land = land
        self.land.resetCanopyHeight()
        self.flora = flora
        print("Initialize a new Bettina setup.")

    def evolveSystem(self):
        self.flora.evolveFlora()
        print(
                "Evolve system at t = " +
                str(int(self.flora.times[0]/(60*60*24))) +
                " days")
        self.land.updateLandMesh()
        self.flora.updateAllMeshes(self.flora.times[-1])
        self.flora.updateFloraCollection()
        self.flora.updateTreeCollection()

    def setVariableSubsurfaceProperties(self, landfiles, directory):
        self.land.setSubsurfaceFileInformation(directory, landfiles)
        self.land.extractSubsurfacePropertiesFromFiles()

    def setConstantSubsurfaceProperties(self, times, properties):
        self.land.setConstantSubsurfaceProperties(times, properties)

    def createFloraCollection(self, prefix, postfix):
        self.flora.createFloraCollection(prefix, postfix)

    def createTreeCollection(self, species_list, postfix):
        self.flora.createTreeCollection(species_list, postfix)
