#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Flora object. This class contains all information about the plant population.
It checks for different competition modi, e.g. shodowing and competition for
belowground resources and passes the information to the individual plants.
Additionally it induces plant specific processes like resource uptake and tree
grwoth/death.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""

import numpy as np
from pybettina import Tree
from pymeshinteraction import MeshInteractor
from pymeshinteraction import SortFileNames as SFN


class Flora:
    def __init__(
            self, name, land, directory, environmental_constants="dummy"):
        self.environmental_constant = environmental_constants
        self.flora_name = name
        self.trees = []
        self.land = land
        self.working_directory = directory

    def evolveFlora(self):
        if self.land.variable_land_properties:
            self.extractAvailableBelowgroundResources()
        else:
            self.getConstantBelowGroundResources()
        #self.checkAboveGroundCompetition()
        self.resourceUptake()  # TODO: BoundaryTrees
        self.evolveTreePopulation()  # TODO: Tree functionalities

    def updateAllMeshes(self, t):
        self.land.updateLand()
        for tree in self.trees:
            tree.updateTreePilotPoint()
            tree.pilot_point.createMeshWithAllGlobalPoints(tree.root_mesh_name)
            tree.pilot_point.mesh.outputMesh(self.working_directory)
        self.land.outputLand()


    def randomlyPlantTreesInRectangularDomain(self, n_species, species,
                                              bounding_box,
                                              tree_resolution=[.1]):
        lx = bounding_box[1] - bounding_box[0]
        ox = bounding_box[0]
        ly = bounding_box[3] - bounding_box[2]
        oy = bounding_box[2]

        for i in range(len(species)):
            species_i = species[i]
            n_species_i = n_species[i]
            trees_planted = 0
            while(trees_planted < n_species_i):
                x, y = np.random.uniform(0, 1, 2) * (lx - ox, ly - oy)
                new_tree = Tree.Tree(
                        x, y, self.land,
                        species_i, trees_planted, self.flora_name)

                new_tree.plantTree(self.working_directory, tree_resolution)
                self.trees.append(new_tree)
                trees_planted += 1

    def resourceUptake(self):
        # TODO: Boundary trees!
        for tree in self.trees:
            tree.water_flux, tree.solar_energy = 0, 0
            tree.available_resources = 0
            for i in range(len(self.times)-1):
                t_ini = self.times[i]
                t_end = self.times[i+1]
                delta_t = t_end-t_ini
                if self.land.variable_land_properties:
                    tree.gatherResources(
                            delta_t, self.salinity_values[i],
                            self.pressure_values[i])
                else:
                    tree.gatherResources(
                            delta_t, self.salinity_values,
                            self.pressure_values)
            tree.available_resources = min(tree.water_flux, tree.solar_energy)

    def evolveTreePopulation(self):
        self.land.resetCanopyHeight()
        indices = []
        i = 0
        if(self.times[0] == 0):
            self.writeFloraToVTU(0)
        for tree in self.trees:
            tree.evolve(self.times[0], self.times[-1])
            #iiii = len(tree.crown_node_ids)
            ##for ii in range(iiii):
            #    jj = tree.crown_node_ids[ii]
            #    self.land.canopy_height[jj].append(tree.crown_node_heights[ii])
            if tree.death:
                indices.append(i)
            i += 1
        for i in indices[::-1]:
            self.trees.pop(i)
        self.updateAllMeshes(self.times[-1])
        self.writeFloraToVTU(self.times[-1])

    def extractAvailableBelowgroundResources(self):
        # [[time,[[conc_name, values], [p_name,values]]], [[...],[[...],..]]
        subsurface_properties = self.land.getSubsurfaceProperties()
        times = []
        salinity_values = []
        pressure_values = []
        for i in range(len(subsurface_properties)):
            times.append(subsurface_properties[i][0])
            salinity_values.append(subsurface_properties[i][1][0][1])
            pressure_values.append(subsurface_properties[i][1][1][1])
        self.times = times
        self.salinity_values = salinity_values
        self.pressure_values = pressure_values

    def getConstantBelowGroundResources(self):
        subsurface_properties = self.land.getSubsurfaceProperties()
        self.times = subsurface_properties[0]
        self.salinity_values = subsurface_properties[1][0]
        self.pressure_values = subsurface_properties[1][1]

    def checkAboveGroundCompetition(self):
        for tree in self.trees:
            number = len(tree.crown_node_ids)
            winner = 0
            for ii in range(number):
                jj = tree.crown_node_ids[ii]
                highest = True
                for iii in range(len(self.land.canopy_height[jj])):
                    if (self.land.canopy_height[jj][iii] >
                            tree.crown_node_heights[ii]):
                        highest = False
                if (highest):
                    winner += + 1
            tree.shadow_coefficient = 1 - winner/float(number)

    def writeFloraToVTU(self, time):
        flora_mesh = MeshInteractor.MeshInteractor(
                self.flora_name + "_grid_t_" + "%010d" % time)
        midpoints = [[], []]
        root_radii, stem_radii, stem_heights, crown_radii = [], [], [], []
        for tree in self.trees:
            midpoints[0].append((tree.tree_center[0], tree.tree_center[1], 0))
            midpoints[1].append(tree.iD)
            root_radii.append(tree.root_radius)
            stem_radii.append(tree.stem_radius)
            stem_heights.append(tree.stem_height)
            crown_radii.append(tree.crown_radius)
        flora_mesh.CreateMeshFromPoints(midpoints)
        flora_mesh.addPropertyVector(root_radii, "r_root", "double")
        flora_mesh.addPropertyVector(stem_radii, "r_stem", "double")
        flora_mesh.addPropertyVector(crown_radii, "r_crown", "double")
        flora_mesh.addPropertyVector(stem_heights, "h_stem", "double")
        flora_mesh.outputMesh(self.working_directory)

    def getAllRootNames(self):
        rootnames = []
        for tree in self.trees:
            rootnames.append(tree.root_mesh_name)
        return rootnames

    def createFloraCollection(self, prefix, postfix):
        self.file_reader_flora = SFN.ReadAndSortFileNames(
                self.working_directory, prefix, postfix)

    def updateFloraCollection(self):
        self.file_reader_flora.createPvDFile(
                self.flora_name + "_meshes" + ".pvd")
        files = self.file_reader_flora.getSortedFiles()
        self.file_reader_flora.addMeshesToPvdFile(files)

    def createTreeCollection(self, species_list, postfix):
        self.file_reader_trees_list = []
        for species in species_list:
            self.file_reader_trees_list.append(
                SFN.ReadAndSortFileNames(self.working_directory,
                                         species, postfix))

    def updateTreeCollection(self):
        i = 1
        for file_reader_trees in self.file_reader_trees_list:
            file_reader_trees.createPvDFile(
                self.flora_name + "_tree_meshes_" + file_reader_trees.prefix
                + ".pvd")
            i += 1
            files = file_reader_trees.getSortedFiles()
            file_reader_trees.addMeshesToPvdFile(files)
