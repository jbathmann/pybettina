#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@date: 2018-Today
@author: jasper.bathmann@ufz.de
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example script in order to demonstrate how bettina might be used. The
parameters are explained below.
@date: 2018 - Today
@author: jasper.bathmann@ufz.de
"""
if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from pybettina import Land
from pybettina import Flora
from pybettina import Bettina
from pybettina import Tree
import numpy as np
import os

# working_directory: Directory, where simulation results are saved.
# Creates a new folder if working_directory has not yet been created.
working_directory = "./tests/"
try:
    os.mkdir(working_directory)
except OSError:
    print("Creation of the directory %s failed" % working_directory)
else:
    print("Successfully created the directory %s " % working_directory)

# model_name: This string is contained in all output files generated
# -- type:string
model_name = "example_model"

# length_x,y: the dimensions (in m) of the simulated land domain
# -- type:double,doube
length_x, length_y, length_z = 12, 6, 1.5
        # land_origin_x,y: the coordinates of the upper-left-bottom corner of the
        # land mesh. The z_coordinate of the upper_left_bottom corner is always set
        # to z = -land_length_z -- type:float
origin_x, origin_y = 0, 0
        # land_layers_x,y,z: the number of layers in each dimension. -- type:int
layers_x, layers_y = 20, 10
# pressure_initial_name, concentration_initial_name: names for the initial con-
# ditions for the primary variables -- type:string
concentration_initial_name = "c_ini"
pressure_initial_name = "p_ini"
# darcy_velocity_initial_name: name of the initial darcy velocity distribution.
# Necessary to define 2nd type boundary conditions -- type:string
darcy_velocity_initial_name = "q_ini"
# node_id_name: name of the property vector containing the node ids. Necessary
# for ogs to run properly. -- type:string
node_id_name = "bulk_node_ids"
# pressure_variable_name, concentration_variable_name: Names of the primary
# variables themselves -- type:string
concentration_variable_name = "concentration"
pressure_variable_name = "pressure"

# tree_species_names: list containing the species which considered in the
# initial tree distribution. Given in the form: [species1, species2, ...],
# with species_i being a string with the species name -- type:list of strings
tree_species_names = ["Avicennia"]
# initial_plants: number of individums planted for each species for the initial
# plant distributions. Given in the form [n_species1, n_species2, ...], with
# n_speciesi being the number of individuums of each species
# -- type:list of strings
initial_plants = [10]

# ini_darcy/pressure/concentration_function(point): functions to create intial
# conditions. Note: all functions must depend on point, which is a (3,1) tuple
# in order to allow for spatially depending initial distributions
#  -- type:python function decleration

# number_of_bettina_timesteps: total number of iterations of the bettina model
# -- type:int

def ini_darcy_function(point):
    return 0


def ini_pressure_function(point):
    return 0


def ini_concentration_function(point):

    return 0.05 * (1 + (np.sqrt((point[0]-length_x/2.)**2 + (point[1]-length_y/4.)**2))/(length_x*1.5))


number_of_bettina_timesteps = 50
# bettina_delta_t: length of one timestep in bettina in [s]. Note: one day
# corresponds to 86400 seconds -- type:double
bettina_delta_t = 356.25/4. * 86400 * 4.

# vtu_file_postfix: last characters of all vtu files -- type:string
vtu_files_postfix = ".vtu"

# # # # # # # # End of parameter definition

# # # Land domain definition
land = Land.Land(model_name + "_land", working_directory)
# # Creation of 2D patch
land.createLand(model_name + "_land_mesh", origin_x,
                                     origin_y,
                                     length_x, length_y,
                                     length_z, layers_x + 1,
                                     layers_y + 1)# # Property naming
land.setCurrentPropertyNames(
        [concentration_variable_name, pressure_variable_name])
# # Output of grid file to hard drive

# # # Plant population definition
flora = Flora.Flora(model_name + "_flora", land, working_directory)
# # Initial plant distribution, here the trees are planted randomly
new_tree = Tree.Tree(3, 3., flora.land, "Avicennia", 0,
                     flora.flora_name)
new_tree.plantTree(flora.working_directory, tree_resolution=[0.8])
flora.trees.append(new_tree)
second_tree = Tree.Tree(3.3, 3, flora.land, "Avicennia", 1,
                        flora.flora_name)
second_tree.plantTree(flora.working_directory, tree_resolution=[0.8])
flora.trees.append(second_tree)
# # Initial conditions for land patch
c, p = land.setCIniPIniAndNodeIds(
        concentration_initial_name, pressure_initial_name,
        darcy_velocity_initial_name, node_id_name,
        ini_darcy_function, ini_pressure_function, ini_concentration_function,
        flora)

flora.updateAllMeshes(0)
#land.outputLand()
c, p = land.getCIniPIni(concentration_initial_name, pressure_initial_name)

# # # Bettina model definition using the land and flora object defined above
bettina = Bettina.Bettina(model_name + "_bettina", land, flora)

bettina.createFloraCollection(model_name + "_flora_grid", vtu_files_postfix)
bettina.createTreeCollection(tree_species_names, vtu_files_postfix)
# # Timeloop for bettina simulation
for i in range(number_of_bettina_timesteps):
    bettina.setConstantSubsurfaceProperties(
            [i * bettina_delta_t, (i + 1) * bettina_delta_t], [c, p])

    bettina.evolveSystem()

